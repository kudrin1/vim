" Set no VI comfortable
set nocompatible

" Show line number
set number

" Set modifable for figutive can make changes to file
set modifiable

" Setup ctags for ruby projects
set tags+=gems.tags

filetype off "required for Vundle
" Vundle automatic vim plugin manager
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
" required! 
Plugin 'gmarik/vundle'
Plugin 'file-line'

" My bundles here:
"
" original repos on GitHub
" use with rails
Plugin 'tpope/vim-rails'
" And padrino framework
Plugin 'spllr/vim-padrino'
" use power status line
Plugin 'Lokaltog/vim-powerline'
Plugin 'scrooloose/syntastic'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
" Add +\- in starat of string when it chabged
Plugin 'airblade/vim-gitgutter'
" Add git commands
Plugin 'tpope/vim-fugitive'
" Add branch info in statusline
Plugin 'taq/vim-git-branch-info'
" Rspec
Plugin 'thoughtbot/vim-rspec'
" Rails bundler
Plugin 'tpope/vim-bundler'
Plugin 'grep.vim'

" Rust language
Plugin 'rust-lang/rust.vim'

" Ack search
Plugin 'mileszs/ack.vim'

" Regular search
Plugin 'nelstrom/vim-visual-star-search'

" Integration ack vim to Nerd Tree
Plugin 'vim-scripts/nerdtree-ack'

" Slim sintax
Plugin 'slim-template/vim-slim.git'
Plugin 'jpo/vim-railscasts-theme'

" Edit markdown
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
set nofoldenable


call vundle#end()

" Enaple syntax from plugin
filetype plugin indent on

" Enable mouse
set mouse=a

" Enable color scheme
" colorscheme railscasts

" Set encoding to UTF-8
set encoding=utf-8

" Enable pathogen infect
" call pathogen#infect()
"
" add jbuilder syntax highlighting
au BufNewFile,BufRead *.json.jbuilder set ft=ruby

" Show status line
set laststatus=2

" Enable 256 color
set t_Co=256

" Powerline fancy style
let g:Powerline_symbols = 'fancy'

" Tab as 2 spaces
set smartindent
set tabstop=2
set shiftwidth=2
set expandtab

" Comment line with ctrl+\
map <C-\> <leader>cl<space>j
imap <C-\> <esc><leader>cl<space>i<down>
vmap <C-\> <leader>cl<space>gv<down>

" Unomment line with alt+\
map <M-\> <leader>cu<space>j
imap <M-\> <esc><leader>cu<space>i<down>
vmap <M-\> <leader>cu<space>gv<down>

" Select with shift in insert mode
imap <S-Up> <esc>V
imap <S-Down> <esc>V
imap <S-Left> <esc>v
imap <S-Right> <esc>v

" Select with shift in normal mode
nmap <S-Up> <esc>V
nmap <S-Down> <esc>V
nmap <S-Left> <esc>v
nmap <S-Right> <esc>v

" On enter copy to buffer and go back to inset mode
vmap <CR> yi

" Select vith holded shift into visual mode
vmap <S-Up> k
vmap <S-Down> j
vmap <S-Right> l
vmap <S-left> h

" Paste with c-p
imap <C-p> <esc>pi

" Format text by  tab
nnoremap <Tab> >>_
nnoremap <S-Tab> <<_
inoremap <S-Tab> <C-D>
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv

" Paste mode
map <F4> :set invpaste paste?<CR>
set pastetoggle=<F4>

" Nerd Tree
imap <F3> <esc>:NERDTreeTabsToggle<CR>i
nmap <F3> <esc>:NERDTreeTabsToggle<CR>
vmap <F3> <esc>:NERDTreeTabsToggle<CR>v

noremap <silent> <F5> :tabprev<CR>
noremap <silent> <F6> :tabnext<CR>

" Vim commander
noremap <silent> <F9> :cal VimCommanderToggle()<CR>

" Pathogen
"execute pathogen#infect()
syntax on
filetype plugin indent on

" backup to ~/.tmp 
set backup 
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp 
set backupskip=/tmp/*,/private/tmp/* 
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp 
set writebackup

" Rspec
map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>
map <Leader>l :call RunLastSpec()<CR>
map <Leader>a :call RunAllSpecs()<CR>

" Disabel replace mode
function s:ForbidReplace()
    if v:insertmode isnot# 'i'
        call feedkeys("\<Insert>", "n")
    endif
endfunction
augroup ForbidReplaceMode
    autocmd!
    autocmd InsertEnter  * call s:ForbidReplace()
    autocmd InsertChange * call s:ForbidReplace()
augroup END
