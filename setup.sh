#!/bin/bash

function message {
 echo -e "\033[37;1;41m$1\033[0m"
}

message "install git, vim, terminator, mc"
sudo dnf -y install git vim terminator mc

message "add symlink to .vimrc"
rm ~/.vimrc
ln -s ~/.vim/.vimrc ~/.vimrc

message "add symlink to .fonts"
rm -rf ~/.fonts
ln -s ~/.vim/.fonts ~/.fonts

message "add symlink to terminator config"
rm -rf ~/.config/terminator/config
ln -s ~/.vim/.config/terminator/config ~/.config/terminator/config

message "install vundle"
rm -rf ~/.vim/bundle/Vundle.vim
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

message "install vim plugins"
vim +BundleInstall +qall

message "add symlink to .ctags"
rm -rf ~/.ctags
ln -s ~/.vim/.ctags ~/.ctags

message "add symlink to .ackrc"
rm -rf ~/.ackrc
ln -s ~/.vim/.ackrc ~/.ackrc

message "add git aliases"
git config --global user.name "Alexander Kudrin"
git config --global user.email kudrin.alexander@gmail.com

git config --global alias.st status
git config --global alias.co checkout
git config --global alias.ci commit
git config --global alias.br branch
git config --global alias.lg "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%Creset' --abbrev-commit --date=relative"
git config --global alias.put "push origin HEAD"
git config --global alias.up "pull --rebase --stat"
git config --global alias.edit-unmerged '!f() { git ls-files --unmerged | cut -f2 | sort -u ; }; vim `f`'
git config --global alias.add-unmerged '!f() { git ls-files --unmerged | cut -f2 | sort -u ; }; git add `f`'
git config --global alias.lc "log ORIG_HEAD.. --stat --no-merges"
