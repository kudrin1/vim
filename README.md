# How to install on local machine
```bash
git clone git@github.com:kudrin/vim.git ~/.vim
~/.vim/setup.sh
```

# How save changes in .vimrc
```bash
cd ~/.vim
git commit -am 'message'
git push
```
